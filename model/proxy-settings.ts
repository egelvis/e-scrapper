/**
 * Clase para definir los settings de proxy
 */
export class ProxySettings {
    constructor(
        /** URL:Puerto del Proxy */
        public ip: string,
        /** User si necesita identificarse */
        public user: string,
        /** password del proxy */
        public password: string

    ) {}
}