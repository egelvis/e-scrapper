import { app, BrowserWindow, Session, TouchBarColorPicker } from "electron";
import { hostname } from "os";
import * as path from 'path';
import * as request from 'request';
import * as fs from 'fs';
import { ProxySettings } from "../model/proxy-settings"
import { UrlToNavigate } from "../model/url-to-navigate"

let mainWindow: BrowserWindow
let ses : Session

let localInstanceValue = `${hostname()}--${Math.floor(Math.random()*1500)}`
const options = { userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 
extraHeaders: 'referer: https://www.google.com/' }
const warmUpUrl = 'http://geo.geosurf.io/'

enum NavigationStatus {
  WarmUp = 1,
  AuthWall = 2,
  HttpStatusError = 3,
  BadGateway = 4,
  ValidNavigation = 5
}

let currentNavigationStatus: NavigationStatus

// Esta data se debera pedir via request y ser actualizada al recibirla
let proxiesAvailable: { Proxies: Array<ProxySettings>; } 
let urlsToNavigate: { Profiles: Array<UrlToNavigate>}

let currentInterval: number = 10000;

//#region Temporary data for testing purposes
let proxyIndex: number = 0;
let stats = {good: 0, authwall: 0, http_errors: 0}

// Load configuration files
fs.readFile('proxySettings.json', (err, data) => {
  if (err) throw new Error('Error al inicializar la configuración: ' + err);
  proxiesAvailable = JSON.parse(data.toString());
})

fs.readFile('urlsToNavigate.json', (err, data) => {
  if (err) throw new Error('Error al obtener las urls a navegar: ' + err);
  urlsToNavigate = JSON.parse(data.toString());
})
//#endregion

function createWindow() {

  // Create the browser window.
  mainWindow = new BrowserWindow({
      width: 800,
      height: 600,
      minimizable: false,
      autoHideMenuBar: true,
    });

  ses = mainWindow.webContents.session

  const cookie = { url: 'https://www.linkedin.com', name: 'lang', value: 'v=2&lang=en-us' }

  ses.cookies.set(cookie)

  mainWindow.webContents.loadURL(warmUpUrl)

  mainWindow.webContents.on('did-navigate', (event, url, httpResponseCode, httpStatusCode)=>{
    console.log(`Url ${url} with statusCode ${httpResponseCode}`)

    if (httpResponseCode === 200)
    {
      if (url === warmUpUrl) 
      {
        currentNavigationStatus = NavigationStatus.WarmUp
      }
      else if (url.includes("authwall") || url.includes("checkpoint")) 
      {
        currentNavigationStatus = NavigationStatus.AuthWall
        stats.authwall++
      }
      else 
      {
        currentNavigationStatus = NavigationStatus.ValidNavigation
        stats.good++
      }
    }
    else if (httpResponseCode === 999) {
      currentNavigationStatus = NavigationStatus.AuthWall
    }
    else if (httpResponseCode === 502) { // Bad Gateway
      currentNavigationStatus = NavigationStatus.BadGateway
    }
    else {
      currentNavigationStatus = NavigationStatus.HttpStatusError
      stats.http_errors++
    }
    
    mainWindow.setTitle(`Stats: Good -> ${stats.good} / HttpErrors -> ${stats.http_errors} / AuthWall -> ${stats.authwall}`)
  })

  mainWindow.webContents.on('did-finish-load', ()=>{ 
    console.log("Did-finish-load "+new Date().toISOString()+" "+currentNavigationStatus)
    if (currentNavigationStatus != NavigationStatus.AuthWall)  
        setTimeout(()=>{
          nextNavigation()
        },currentInterval)
  })

  mainWindow.webContents.on('login', (event, authenticationResponseDetails, authInfo, callback) => {
    console.log(`Login ${authenticationResponseDetails.url} with authInfo ${authInfo.host} ${currentNavigationStatus}`)
      if (authInfo.isProxy) {
        callback(proxiesAvailable.Proxies[proxyIndex].user, proxiesAvailable.Proxies[proxyIndex].password);
      }
      event.preventDefault();
  });

  mainWindow.webContents.on('will-navigate', (event, url)=>{
    console.log("Url redirected with window.location "+url+" "+currentNavigationStatus)
    setTimeout(()=>{
      nextNavigation()
    },currentInterval)
  })

  mainWindow.webContents.on('will-redirect', (event, url)=>{
    console.log("Url redirected (302) "+url)
    setTimeout(()=>{
      nextNavigation()
    },currentInterval)
  })

  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
   mainWindow.on("closed", () => {
   mainWindow = null;
  });
}

app.disableHardwareAcceleration()

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", ()=> {
  console.log("Llamada a Ready")
  createWindow()
});

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
    console.log(`Stats: Good -> ${stats.good} / HttpErrors -> ${stats.http_errors} / AuthWall -> ${stats.authwall}`)
  }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.

function nextNavigation()
{
  if (currentNavigationStatus == NavigationStatus.ValidNavigation)
    extractDataFromHtml()

  // Requires activate Proxy
  if (currentNavigationStatus == NavigationStatus.WarmUp || 
      currentNavigationStatus == NavigationStatus.AuthWall || 
      currentNavigationStatus == NavigationStatus.BadGateway)
      setProxy()

  setTimeout(()=>{
    nextProfile()
  }, 2000)
}

function setProxy() {
  ses.clearCache()
  mainWindow.webContents.clearHistory()
  proxyIndex = Math.floor(Math.random() * proxiesAvailable.Proxies.length)
  let proxyConfig = { proxyRules: proxiesAvailable.Proxies[proxyIndex].ip };
  ses.setProxy(proxyConfig);
  ses.resolveProxy(proxiesAvailable.Proxies[proxyIndex].ip)
  console.log(`Finish setting proxy ${proxiesAvailable.Proxies[proxyIndex].ip} ${currentNavigationStatus}`)
}

function nextProfile(){
  // Este dato debe ser solicitado al endpoint
  console.log(`Next Profile ${new Date().toISOString()} ${currentNavigationStatus}`)
  currentNavigationStatus = NavigationStatus.ValidNavigation
  let index = Math.floor(Math.random()*urlsToNavigate.Profiles.length)
  let url = urlsToNavigate.Profiles[index].url
  mainWindow.webContents.loadURL(url, options)
}

function extractDataFromHtml(){
  var now = new Date();
  let dateStr = now.getTime()
  let file_name = `${app.getPath('desktop')}\\Profiles\\${dateStr}_${Math.floor(Math.random()*50000)}.html`
  console.log(`Creating File ${file_name} [${new Date().toISOString()}] ${currentNavigationStatus}`)
  mainWindow.webContents.savePage(file_name, "HTMLOnly")
}


