  ___            _____                                      
 |  _|         / ____|                                     
 | |_          | (___   ___ _ __ __ _ _ __  _ __   ___ _ __ 
 |  _| _______ \___ \ / __| '__/ _` | '_ \| '_ \ / _ \ '__|
 | |__         ____) | (__| | | (_| | |_) | |_) |  __/ |   
 |____|       |_____/ \___|_|  \__,_| .__/| .__/ \___|_|   
                                    | |   | |              
                                    |_|   |_|              

                ~ Guía de instalación y compilado ~

        +------------+
-=-=-=- | Requisitos | -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        +------------+

Para poder compilar este proyecto hace falta tener instalado:

  node.js
  npm

        +--------------------------------------+
-=-=-=- | Configuración básica (version larga) | -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        +--------------------------------------+

Al clonar este repositorio, hará falta configurar algunas cosas antes de poder
comenzar a trabajar. Como recién está clonado, todavía no tendremos instalados
los paquetes de npm que requiere. Para instalarlos vamos a:

  ultrascrapperapp/Source/UltraScrapperApp

Abrimos Powershell en este directorio (cosa que se puede hacer escribiendo
"powershell" en la barra de direcciones del explorador de Windows) y allí
ejecutamos:

  npm install

Para que baje los paquetes de npm que faltan. Estos módulos no están subidos
al repositorio por dos motivos: 1, ocupan mucho espacio y no tiene sentido
andarlos guardando; y 2, UltraScrapper funciona en Linux, Windows y Mac y,
si queremos hacer un build en Linux y clonamos el repositorio con los paquetes
compilados para Windows, no va a funcionar.

UltraScrapper está hecho en Electron con Angular. Los archivos a destacar
dentro de ultrascrapperapp/Source/UltraScrapperApp son:

  * package.json: es donde viene qué node modules estamos usando para el
    desarrollo, cuáles para el build, hay configuraciones, comandos npm hechos
    por nosotros, etc.
  * package-lock.json: se genera automaticamente cada vez que instalamos un
    módulo nuevo (y se actualiza).
  * semaint.json: es el archivo de configuración del semantic UI que usamos para
    hacer la interfaz gráfico.
  * Los archivos que comienzan con "ts" son archivos de configuración de
    TypeScript.
  * angular.json: es el archivo de configuración de Angular.
  * .editorconfig: setup de configuración del proyecto para Visual Studio Code.

UltraScrapper está escrito en TypeScript. TypeScript hace de JavaScript un
lenguaje tipado. Tiene muchas ventajas: entre ellas, la herencia de clases es
muy parecida a la de C#, y no por prototipos como JS.

En el directorio ultrascrapperapp/Source/UltraScrapperApp/src tenemos el código
propiamente dicho de la aplicación, que está escrito en Angular. Todas las
demás subcarpetas de este directorio son carpetas necesarias para que funcione
el programa, con módulos y bibliotecas.

Semantic es un paquete npm que no requiere actualización así que se ha agregado
al repositorio. e2e es para las pruebas end-to-end, que en general se tienen que
hacer y estamos haciendo una suite para probar cosas. En build acaba el build y
angular_build es lo mismo para Angular.

Ejecutar "npm audit fix" para resolver algunas vulnerabilidades. No resuelve
todas, pero las vulnerabilidades que tenemos actualmente son algunas que
decidimos no arreglar porque necesitamos esas versiones de los node modules que
bajamos; así que hay aproximadamente 7 vulnerabilidades que ya sabemos que
tenemos y no deben preocuparnos. En general son por la versión vieja de
Semantic que estamos usando (pero que es la que anda bien con Angular).

Lo siguiente que hay que hacer es correr:

  npm run cleanwin

Que es un comando propio nuestro ('cleanwin' para una máquina con Windows). Este
comando lo que hace es borrar una carpeta. En un node-module que usamos, los
devs decidieron poner otra vez como dependencia a Node entonces acabamos con
archivos de Node en dos lugares diferentes y esto hace que el proyecto no
compile, por lo que la borramos con esto. En Linux reemplazamos 'cleanwin' por
'cleanlinux'.

Dejamos Powershell abierto por ahora.

        +-------------------+
-=-=-=- | Ejecutar las APIs | -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        +-------------------+

Clonar el repositorio de UltraScrapperWebApiDotNet de:

  https://bitbucket.org/bairesdevrd/ultrascrapperwebapidotnet/

Este contiene el backend de la aplicación. Abro la solución con Visual Studio
haciendo doble click en "UltraScrapperBackendAPI.sln". Para poder ejecutar
esto tenemos que hacer dos cosas. Modificar el WebConfig con nuestros datos
para que se conecte a la base de datos de Umbrella. Además, este proyecto tiene
una dependencia de UmbrellaDal. Para esto tenemos que clonar Umbrella desde:

  https://bitbucket.org/bairesdevrd/umbrella

Y luego hay que compilar UmbrellaDal.sln, contenido en Umbrella/Dal/UmbrellaDal.

Hecho esto, ejecutamos el backend, que correrá en localhost:<puerto>. Luego
hemos de ejecutar la web API intermedia, UltraScrapperBackendAPI. Para esto
debemos clonar su repositorio desde:

  https://bitbucket.org/bairesdevrd/ultrascrapperwebapi/

Una vez clonada abrimos:

  ultrascrapperwebapi/Source/UltraScrapperWebAPI.sln

con Visual Studio y antes de echarla a andar tengo que ver que en el archivo
de configuración esté seteado el mismo puerto en el que quedó corriendo el
backend. Modificamos "UmbrellaWebApiSettings/WebApiUrl" en el archivo
"appsettings.json" con el puerto correcto.

Hecho esto, ejecutamos esta API también, que nos contestará por
localhost:<otropuerto> (distinto al puerto anterior). Tomamos nota de este
puerto.

Ahora podemos ejecutar UltraScrapper.

        +--------------------------+
-=-=-=- | Ejecutando UltraScrapper | -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        +--------------------------+

Vamos a "ultrascrapperapp/Source", hacemos click derecho sobre el directorio
UltraScrapperApp y lo abrimos con Visual Studio Code ("Open with Code"). Esto
nos abrirá el proyecto en el editor. En la lista de carpetas que aparece a la
izquierda, vamos a src/app/services y abrimos api.service.ts.

En el archivo api.service.ts modificamos el valor de la variable "url" de la 
clase ApiService y ponemos el la dirección y el puerto en los que está
corriendo la API intermedia (localhost:<otropuerto>).

Ahora podemos compilar. Volvemos a Powershell y corremos "npm run electron".
Siempre la primera vez que se compila luego de haber bajado el repo tarda un
poco más en compilar que de costumbre.

        +-----------------------------------+
-=-=-=- | Más información sobre el proyecto | -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        +-----------------------------------+

Los contenidos de este documento han sido compilados del video de la 
Introducción técnica a UltraScrapperApp, por Gabo (Gabriel Antonioli) y Lucas 
Muñoz. Puede encontrarse en:

  https://bdev.zoom.us/recording/play/u04qRocPXrIoU-LzPaqcQIrScldrK3bQIEDasVVRPZynC-wO-QJn8KJWqjE7yz3I

El video contiene también información sobre Electron, Angular, las diferentes
partes del proyecto, etc.